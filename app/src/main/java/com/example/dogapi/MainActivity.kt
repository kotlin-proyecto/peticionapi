package com.example.dogapi

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import com.example.dogapi.modelo.PokeAPI
import com.example.dogapi.ui.theme.DogAPITheme
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MainActivity : ComponentActivity() {

    private lateinit var retrofit : Retrofit
    private var texto: String = "Pruebas"


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        retrofit = retrofit2.Retrofit.Builder()
            .baseUrl("https://dog.ceo/api/breeds/image/random")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        texto = obtenerURL(retrofit)

        setContent {
            DogAPITheme {
                // A surface container using the 'background' color from the theme
                Surface() {
                    Greeting(texto)
                }
            }
        }
    }

    private fun obtenerURL(retrofit : Retrofit) : String{
        var texto = "";
        CoroutineScope(Dispatchers.IO).launch {
            val call =
                retrofit.create(PokeAPI::class.java).getUrl().execute()
            val perrosUrl = call.body()
            if(call.isSuccessful){
                texto = perrosUrl?.getUrl().toString()
            }else{
                texto = "Ha habido un error"
            }
        }
        Thread.sleep(1000)
        return texto;
    }

    private fun obtenerStatus(retrofit : Retrofit) : String{
        var texto = "";
        CoroutineScope(Dispatchers.IO).launch {
            val call =
                retrofit.create(PokeAPI::class.java).getStatus().execute()
            val perros = call.body()
            if(call.isSuccessful){
                texto = perros?.getStatus().toString()
            }else{
                texto = "Ha habido un error"
            }
        }
        Thread.sleep(1000)
        return texto;
    }
}

@Composable
fun Greeting(texto : String) {
    Column {
        Text(text = texto)
    }
}

////





/////////
@Preview(showBackground = true)
@Composable
fun GreetingPreview() {
    DogAPITheme {
        Greeting("Android")
    }
}