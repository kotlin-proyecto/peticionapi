package com.example.dogapi.modelo

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Headers

interface PokeAPI {
    @Headers("Accept: application/json")
    // Método para url de la imagen
    @GET("message")
    fun getUrl(): Call<Pokemon>
    // Método para obtener estado
    @GET("status")
    fun getStatus(): Call<Pokemon>
}
