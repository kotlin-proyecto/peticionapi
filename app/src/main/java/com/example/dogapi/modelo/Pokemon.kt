package com.example.dogapi.modelo

class Pokemon {
    private lateinit var status: String
    private lateinit var url: String
    fun getStatus() : String{
        return this.status
    }
    fun getUrl() : String {
        return this.url
    }
    fun setStatus(status : String){
        this.status = status
    }
    fun setUrl(url : String){
        this.url = url
    }
}